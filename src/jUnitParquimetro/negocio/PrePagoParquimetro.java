package jUnitParquimetro.negocio;

public class PrePagoParquimetro {
	private Parquimetro parq;
	private long totalCredito = 0;
	
	public PrePagoParquimetro(Parquimetro p) {
		parq = p;
	}
	
	public long getCreditos() {
		return totalCredito;
	} 
	
	public long inseriCredito(long cr) {
		if (cr % 10 != 0)
			throw new IllegalArgumentException("Valor n�o � m�ltiplo de 10");
		else {
			totalCredito += cr;
		}
		
		return totalCredito;
	}
	
	public long emiteTicket() {
		boolean res = false;
		
		if (totalCredito >= 2) {
			parq.insereMoeda(100);
			parq.insereMoeda(100);
			res = parq.emiteTicket();
			if (res == true)
				totalCredito-=2;
		}
		else
			throw new IllegalStateException("Cr�dito insuficiente");
		
		return totalCredito;
	}
}
