package jUnitParquimetro.testes;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import jUnitParquimetro.negocio.Parquimetro;
import jUnitParquimetro.negocio.PrePagoParquimetro;

class PrePagoParquimetroJU5 {

	private Parquimetro parq;
	private PrePagoParquimetro prePago;
	
	@BeforeEach
	void setUp() {
		parq = Mockito.mock(Parquimetro.class);
		prePago = new PrePagoParquimetro(parq);
	}

	@Test
	void inseriCreditoTest() {
		// Este teste s� necessita da classe "Mock" para ser passada no construtor
		// mas n�o � preciso modelar comportamentos de m�todos
		prePago.inseriCredito(10);
		prePago.inseriCredito(40);
		prePago.inseriCredito(100);
		
		assertEquals(150, prePago.getCreditos());
	}

	@Test
	void inseriCreditoInvalidoTest() {
		assertThrows(IllegalArgumentException.class, 
				() ->  prePago.inseriCredito(11));
	}
	
	@Test
	void emiteTicketTest() {
		long res = 0;
		
		prePago.inseriCredito(10);
		
		when(parq.emiteTicket()).thenReturn(true).thenReturn(true).
									thenReturn(true).thenReturn(true).
									thenReturn(true);
		
		// M�todos do parquimetro que s�o chamados n�o tem retorno.
		// Se falhar, lan�a exce��o. Se n�o lan�ou, tudo ok.
		res = prePago.emiteTicket();
		res = prePago.emiteTicket();
		res = prePago.emiteTicket();
		res = prePago.emiteTicket();
		res = prePago.emiteTicket();

		assertEquals(0, prePago.getCreditos());
	}

	@Test
	void emiteTicketInvTest() {
		assertThrows(IllegalStateException.class,
				() -> prePago.emiteTicket());
	}
}
