package interfaceEx.ligadesliga;

import java.util.ArrayList;

public class ControlaEltros {
    public static void main(String[] args) {
        ArrayList<Controlavel> lista = new ArrayList<Controlavel>();
        
        lista.add(new Tv());
        lista.add(new Tv());
        lista.add(new Tv());

        lista.add(new Lustre(6));
        lista.add(new Lustre(12));
        lista.add(new Lustre(24));

        //Vamos ligar todo mundo

        for (Controlavel controlavel : lista) {
            controlavel.ligar();
        }

        // Vamos ver quem está ligado
        int cont = 0;
        for (Controlavel controlavel: lista)
            if (controlavel.isLigada())
                System.out.println("Ligado " + cont++);
            else
                System.out.println("Desligado "+ cont++);

        // Vamos desligar todo mundo
        for (Controlavel controlavel : lista) {
            controlavel.desligar();
        }

        // Vamos ver quem está ligado
        cont = 0;
        for (Controlavel controlavel: lista)
            if (controlavel.isLigada())
                System.out.println("Ligado " + cont++);
            else
                System.out.println("Desligado "+ cont++);
    }
}
