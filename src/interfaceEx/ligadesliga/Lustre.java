package interfaceEx.ligadesliga;

import java.util.ArrayList;
import java.util.List;

public class Lustre implements Controlavel {
    private int nroLampadas;
    private List<Lampada> lampadas = new ArrayList<Lampada>();
    private boolean ligado;

    public Lustre(int nroLampadas) {
        this.nroLampadas = nroLampadas;
        for (int i = 0; i < nroLampadas; i++) {
            lampadas.add(new Lampada());
        }

        ligado = false;
    }

    public int getNroLampadas() {
        return nroLampadas;
    }

    @Override
    public void ligar() {
        for (Lampada lampada : lampadas) {
            lampada.ligar();
        }
        ligado = true;
    }
    @Override
    public void desligar() {
        for (Lampada lampada : lampadas) {
            lampada.desligar();
        }
        ligado = false;
    }
    
    @Override
    public boolean isLigada() {
        return ligado;
    }
}
