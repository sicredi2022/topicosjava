package interfaceEx.ligadesliga;

public class Tv implements Controlavel{
    private int fonte;
    private String tela;

    @Override
    public void ligar() {
       fonte = 220;
       tela =  "on";
    }

    @Override
    public void desligar() {
        tela = "off";
        fonte = 0;
    }

    @Override
    public boolean isLigada() {
        boolean res = false;

        if (fonte == 220 && tela.equals("on"))
            res = true;
        return res;
    }
    
}
