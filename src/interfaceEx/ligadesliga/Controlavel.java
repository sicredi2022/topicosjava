package interfaceEx.ligadesliga;

public interface Controlavel {
    public void ligar();
    public void desligar();
    public boolean isLigada();
}
