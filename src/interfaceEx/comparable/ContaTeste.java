package interfaceEx.comparable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ContaTeste {
    public static void main(String [] argc){
        List<String> l1 = new ArrayList<String>();
        Conta c1, c2;
        ContaCorrente cc1, cc2;
        int res = "abc".compareTo("aaa");
        
        
        l1.add("c");
        l1.add("a");
        l1.add("d");
        l1.add("b");
        
        Collections.sort(l1);

        c1 = new Conta();
        c2 = new ContaCorrente();

        cc1 = new ContaCorrente();
        cc2 = (ContaCorrente) c2;

        res = c1.compareTo(c2);

        cc2.quemSou();



    }
}
