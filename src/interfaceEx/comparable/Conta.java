package interfaceEx.comparable;

public class Conta implements Comparable<Conta> {
    private String nome;
    
    public void setNome(String n){
        nome = n;
    }

    public String getNome(){
        return nome;
    }

    @Override
    public int compareTo(Conta o) {
          return nome.compareTo(o.getNome());
    }
}
