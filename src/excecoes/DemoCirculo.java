package excecoes;

public class DemoCirculo {
    public static void main(String[] args) {
        Circulo c1, c2;
        try {
            c1 = new Circulo(10, 10, 100);
            System.out.println("Instanciou c1!");

            c2 = new Circulo(-1, 10, 200);
            System.out.println("Instanciou c2!");

            System.out.println("Instanciou tudo!");
        } catch (IllegalArgumentException ex) {
            System.out.println("Problemas na instanciação:");
            System.out.println(ex.getMessage());
        }
    }
}
