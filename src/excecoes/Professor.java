package excecoes;

public class Professor {
    private String nome;
    private ProfTitulacao titulacao;

    Professor(String n, ProfTitulacao t) {
        nome = n;
        titulacao = t;
    }

    public String getNome() {
        return nome;
    }

    public ProfTitulacao getTitulacao() {
        return titulacao;
    }
}
