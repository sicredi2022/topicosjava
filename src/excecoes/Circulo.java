package excecoes;

public class Circulo {
    private int centrox;
    private int centroy;
    private int raio;

    public Circulo(int x, int y, int r) {
        if (x < 0) {
            IllegalArgumentException excecao = new IllegalArgumentException("Valor do centrox negativo");
            throw excecao;
        } 
        if (y < 0) {
            IllegalArgumentException excecao = new IllegalArgumentException("Valor do centroy negativo");
            throw excecao;
        }
        if (r <= 0) {
            IllegalArgumentException excecao = new IllegalArgumentException("Valor do raio não positivo");
            throw excecao;
        }
        centrox = x;
        centroy = y;
        raio = r;
    }
}