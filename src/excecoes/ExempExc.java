package excecoes;

import java.io.IOException;

import javax.swing.JOptionPane;

public class ExempExc {
    public static void main(String[] args) {
        int quantidade;
        String s = JOptionPane.showInputDialog("Digite um valor inteiro:");
        try {
            // método parseInt() pode gerar exceção
            quantidade = Integer.parseInt(s);
            System.out.println(quantidade);
        }
        catch (NumberFormatException e) {
        // código para tratar a exceção
            System.out.println("Erro de conversão");
        }
       
        System.out.println("Fim do programa");
    }
}
