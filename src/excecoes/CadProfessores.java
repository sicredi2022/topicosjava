package excecoes;

import java.util.ArrayList;

public class CadProfessores {
    private ArrayList<Professor> professores = new ArrayList<Professor>();

    public void adiciona(Professor p) {
        professores.add(p);
    }

    public void removeInicio() {
        professores.remove(0);
    }

    public int indiceDe(Professor p) {
        for (int i = 0; i < professores.size(); i++) {
            if (professores.get(i).getNome().equals(p.getNome())) {
                return i;
            }
        }

        return -1;
    }

}
