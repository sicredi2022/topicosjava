package excecoes;

public class DemoVerficada {
	public void f1() throws InterruptedException {
		// public void f1() {
		System.out.println("Espere um pouco ...");
		// try {
		Thread.sleep(2000);
		// } catch (InterruptedException e) {
		// }

		System.out.println("Obrigado");
	}

	public static void main(String args[]) {
		DemoVerficada dv = new DemoVerficada();
		try {
			for (int i = 0; i < 10; i++) {
				dv.f1();
			}

		} catch (InterruptedException e) {

		}
	}

}
