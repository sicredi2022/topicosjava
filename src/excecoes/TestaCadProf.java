package excecoes;

public class TestaCadProf {
    public static void main(String[] args) {
        CadProfessores cadastro = new CadProfessores();
        Professor p1 = new Professor("João", ProfTitulacao.GRADUACAO);
        Professor p2 = new Professor("Maria", ProfTitulacao.MESTRADO);
        Professor p3 = new Professor("José", ProfTitulacao.DOUTORADO);
        try {
            cadastro.adiciona(p1);
            cadastro.adiciona(p2);
            cadastro.adiciona(p3);
            cadastro.removeInicio();
            cadastro.removeInicio();
            cadastro.removeInicio();
            cadastro.removeInicio();
            System.out.println(cadastro.indiceDe(p3));
        } catch (IndexOutOfBoundsException e) {
            System.out.println("Erro: " + e.getMessage());
        }
        
    }
}
